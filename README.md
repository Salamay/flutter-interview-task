# Flutter Interview Task

## Interview Task

Build a Dog Social Application using this [design spec](https://www.figma.com/file/bSQMB5hImn2F44fmsr9559/Flutter-Interview-Task?node-id=0%3A1).

There is no need to build any backend (api layer, server, database). For simulating form submission, you can do a POST to https://hookb.in/mZZ8pmBdk6ilzXNNzQGp

- On the Dashboard, we expect that the rows scroll horizontally

## Duration

Up to 8 hours. We do not expect you to complete the assessment in this time.

## Submission
I was unable to complete making the UI because the time wasnt enough. If i was given more time, i would make the UI look much impressive.