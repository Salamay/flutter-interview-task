import 'package:bottom_bar/bottom_bar.dart';
import 'package:dog/Screens/Profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'HomePage.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _pageController = PageController();
  final _key = GlobalKey<ScaffoldState>();
  int index = 0;

  @override
  Widget build(BuildContext context) {
    double HEIGTH = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: [HomePage(), Container(), Container(), Profile()],
        onPageChanged: (index) {
          // Use a better state management solution
          // setState is used for simplicity
          this.index = index;
          setState(() {});
        },
      ),
      bottomNavigationBar: BottomBar(
        height: HEIGTH * 0.12,
        selectedIndex: index,
        onTap: (int index) {
          _pageController.jumpToPage(index);
          index = index;
          setState(() {});
        },
        items: <BottomBarItem>[
          BottomBarItem(
            inactiveColor: Colors.grey,
            darkActiveColor: Colors.grey,
            activeColor: Colors.black,
            icon: Container(
              child:  SizedBox(
                  height: HEIGTH * 0.06,
                  width: width * 0.07,
                  child: Image.asset("assets/Home.png"))
            ),
            title:
                Text('HOME', style: TextStyle(color: Colors.grey, fontSize: 8)),
          ),
          BottomBarItem(
            inactiveColor: Colors.grey,
            darkActiveColor: Colors.grey,
            activeColor: Colors.black,
            icon: Icon(
              Icons.people,
              color: Colors.grey,
            ),
            title: Text('Moments',
                style: TextStyle(color: Colors.grey, fontSize: 8)),
          ),
          BottomBarItem(
            inactiveColor: Colors.grey,
            darkActiveColor: Colors.grey,
            activeColor: Colors.black,
            icon: Container(
              child: Column(
                children: [
                  SizedBox(
                      height: HEIGTH * 0.06,
                      width: width * 0.07,
                      child: Image.asset("assets/Send.png"))
                ],
              ),
            ),
            title: Text(
              'Chat',
              style: TextStyle(color: Colors.grey, fontSize: 8),
            ),
          ),
          BottomBarItem(
            inactiveColor: Colors.grey,
            darkActiveColor: Colors.grey,
            activeColor: Colors.pink,
            icon: Icon(
              Icons.person_outline_sharp,
              color: Colors.grey,
            ),
            title: Text('Profile',
                style: TextStyle(color: Colors.grey, fontSize: 8)),
          ),
        ],
      ),
    );
  }
}
