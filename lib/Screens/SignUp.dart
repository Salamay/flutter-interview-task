import 'package:dog/Widget/RoundButton.dart';
import 'package:dog/Widget/constant.dart';
import 'package:flutter/material.dart';
class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _key=GlobalKey<FormState>();
  TextEditingController _fullnameController;
  TextEditingController _emailController;
  TextEditingController _passwordController;
  bool isobscure=true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fullnameController=TextEditingController();
    _emailController=TextEditingController();
    _passwordController=TextEditingController();
  }
  @override
  Widget build(BuildContext context) {
    double HEIGTH = MediaQuery.of(context).size.height;
    double WIDTH = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 20,
          ),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: HEIGTH,
          width: WIDTH,
          child: Form(
            key: _key,
            child: Container(
              padding: EdgeInsets.all(5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Let's start here",
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: 34,
                      letterSpacing: 2,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                  Text(
                    "Fill in your details to begin",
                    style: TextStyle(
                      color: Colors.black54,
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(height: 20,),
                  TextFormField(
                    controller: _fullnameController,
                    decoration: inputdecoration.copyWith(hintText: "Fullname"),
                    validator: (val)=>val.isEmpty?"Please provide your fullname":null,
                  ),
                  SizedBox(height: 20,),
                  TextFormField(
                    controller: _emailController,
                    decoration: inputdecoration.copyWith(hintText: "Email",),
                    validator: (val)=>!RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$').hasMatch(val)?"Invalid email":null,
                  ),
                  SizedBox(height: 20,),
                  TextFormField(
                    obscureText: isobscure,
                    controller: _passwordController,
                    decoration: inputdecoration.copyWith(hintText: "Password",suffixIcon: GestureDetector(
                      onTap: (){
                        isobscure=!isobscure;
                        setState(() {

                        });
                      },
                      child: Icon(
                        Icons.remove_red_eye,
                        color: Colors.grey,
                      ),
                    )),
                    validator: (val)=>val.isEmpty?"invalid password":null,
                  ),
                  SizedBox(height: 20,),
                  FlatButton(
                    minWidth: WIDTH,
                    height: HEIGTH*0.08,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))
                    ),
                    color: Colors.deepOrange[200],
                    onPressed: (){
                      if(_key.currentState.validate()){
                        Navigator.pushNamed(context, "/home");
                      }

                    },
                    child: Text(
                      "Sign up",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  SizedBox(height: HEIGTH*0.3,),
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "by signing in, I agree with",
                              style: TextStyle(color: Colors.grey),
                            ),
                            Text(
                              "Terms of use",
                              style: TextStyle(color: Colors.black),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "and ",
                              style: TextStyle(color: Colors.black),
                            ),
                            Text(
                              "Privacy policy",
                              style: TextStyle(color: Colors.black),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ),
        ),
      ),
    );
  }
}
