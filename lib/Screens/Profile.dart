import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
class Profile extends StatefulWidget {

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  PanelController _panelController=PanelController();
  @override
  Widget build(BuildContext context) {
    double HEIGTH = MediaQuery.of(context).size.height;
    double WIDTH = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: HEIGTH*0.6,
            width: WIDTH,
            child: Image.asset("assets/IMAGE.png",fit: BoxFit.cover,),
          ),
          Positioned(
            left: WIDTH*0.05,
            top: HEIGTH*0.05,
            child: Container(
              color: Colors.transparent,
              child: Image.asset("assets/CLOSE.png",fit: BoxFit.cover,)
            ),
          ),
          Positioned(
            right: WIDTH*0.05,
            top: HEIGTH*0.05,
            child: Container(
                color: Colors.transparent,
                child: Image.asset("assets/VERIFIED.png",fit: BoxFit.cover,)
            ),
          ),
          SlidingUpPanel(
            controller: _panelController,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
            backdropEnabled: true,
            parallaxEnabled: true,
            isDraggable: true,
            minHeight: HEIGTH * 0.4,
            maxHeight: HEIGTH,
            onPanelSlide: (val) {
              //  appBarController.forward();
            },
            panel: Container(
              padding: EdgeInsets.all(10),
              color: Colors.transparent,
              height: HEIGTH * 0.4,
              child: Column(
                children: [
                  Text(
                    "Alex Murray",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 40,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "4/hr"
                      ),
                      Divider(color: Colors.black,thickness: 2,),
                      Text(
                        "10km"
                      ),
                      Divider(color: Colors.black,thickness: 2,),
                      Text(
                        "4.4"
                      ),
                      Divider(color: Colors.black,thickness: 2,),
                      Text(
                        "450 walks"
                      ),
                      Divider(color: Colors.black,thickness: 2,)
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ) ,
    );
  }
}
