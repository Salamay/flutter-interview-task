import 'package:dog/Widget/RoundButton.dart';
import 'package:flutter/material.dart';

class OnBoarding extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double HEIGTH = MediaQuery.of(context).size.height;
    double WIDTH = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.grey,
      body: SafeArea(
        child: Container(
          height: HEIGTH,
          width: WIDTH,
          child: Stack(
            children: [
              Container(
                height: HEIGTH,
                width: WIDTH,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.transparent, Colors.black87])),
                child: Image.asset(
                  "assets/ONBOARDING PICTURE.png",
                  fit: BoxFit.cover,
                ),
              ),
              Positioned(
                  top: HEIGTH * 0.1,
                  left: WIDTH * 0.1,
                  child: Container(
                    height: HEIGTH * 0.1,
                    width: WIDTH * 0.3,
                    child: Row(
                      children: [
                        Image.asset(
                          "assets/log.png",
                          fit: BoxFit.cover,
                        ),
                        Column(
                          children: [
                            Text(
                              "Woo",
                              style: TextStyle(
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.deepOrange),
                            ),
                            Text(
                              "Dog",
                              style: TextStyle(
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.deepOrange),
                            )
                          ],
                        )
                      ],
                    ),
                  )),
              Positioned(
                  bottom: HEIGTH * 0.1,
                  child: Container(
                    width: WIDTH,
                    alignment: Alignment.center,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            "Too tired to help your dog?\nLet's help you!",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                                letterSpacing: 2),
                          ),
                          alignment: Alignment.center,
                        ),
                        RoundButton(
                          onpressed: (){
                            Navigator.pushNamed(context, "/signup");
                          },
                          bColor: Colors.deepOrange[200],
                          tColor: Colors.white,
                          text: "Join our community",
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Already a member?",
                              style: TextStyle(
                                color: Colors.white,

                              ),
                            ),
                            Container(
                              alignment: Alignment.center,
                              child: FlatButton(
                                  onPressed: (){

                                  },
                                  child: Text(
                                    "Sign in",
                                    style: TextStyle(
                                      color: Colors.deepOrange
                                    ),
                                  )
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
