
import 'package:dog/Widget/constant.dart';
import 'package:flutter/material.dart';
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    double HEIGTH = MediaQuery.of(context).size.height;
    double WIDTH = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Container(
              height: HEIGTH,
              width: WIDTH,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: HEIGTH*0.04,),
                  ListTile(
                    trailing:  FlatButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)
                      ),
                        color: Colors.deepOrange,
                        onPressed: (){},
                        child: Text(
                            "Book a walk",
                          style: TextStyle(
                            color: Colors.white
                          ),
                        )
                    ),
                    title: Text(
                      "Home",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 40,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    subtitle: Text(
                      "Book a dog walk",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 20,

                          fontWeight: FontWeight.normal
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  TextField(
                    decoration: inputdecoration.copyWith(hintText: "Kiv, ukraine",prefixIcon: Icon(Icons.location_off,color: Colors.grey,),suffixIcon: Icon(
                        Icons.filter_list_rounded,
                      color: Colors.grey,
                    ),),
                  ),
                  SizedBox(height: 15,),
                  ListTile(
                    title: Text(
                      "Near you",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 40,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    trailing: Text(
                      "view all",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  Container(
                    height: HEIGTH*0.3,
                    width: WIDTH,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: nearYou.length,
                        itemBuilder: (context,index){
                          return Container(
                            height: HEIGTH*0.3,
                            width: WIDTH*0.6,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.asset(nearYou[index].image,fit: BoxFit.cover,),
                                ),
                                ListTile(
                                  title: Text(
                                    nearYou[index].title,
                                    style: TextStyle(
                                      color: Colors.black,
                                      letterSpacing: 2
                                    ),
                                  ),
                                  subtitle: Text(
                                    nearYou[index].subtitle,
                                    style: TextStyle(
                                        color: Colors.black,
                                        letterSpacing: 2
                                    ),
                                  ),
                                  trailing:  ClipRRect(
                                    child: Container(
                                      child: Text(
                                        "3/hr",
                                        style: TextStyle(
                                            color: Colors.white
                                        ),
                                      ),
                                      width: 40,
                                      color: Colors.black,
                                    ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  
                                )
                              ],
                            ),
                          );
                        }
                    ),
                  ),
                  ListTile(
                    title: Text(
                      "Suggested",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 40,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    trailing: Text(
                      "view all",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  Container(
                    height: HEIGTH*0.3,
                    width: WIDTH,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: nearYou.length,
                        itemBuilder: (context,index){
                          return Container(
                            height: HEIGTH*0.3,
                            width: WIDTH*0.6,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.asset(Suggested[index].image,fit: BoxFit.cover,),
                                ),
                                ListTile(
                                  title: Text(
                                    Suggested[index].title,
                                    style: TextStyle(
                                        color: Colors.black,
                                        letterSpacing: 2
                                    ),
                                  ),
                                  subtitle: Text(
                                    Suggested[index].subtitle,
                                    style: TextStyle(
                                        color: Colors.black,
                                        letterSpacing: 2
                                    ),
                                  ),
                                  trailing:  ClipRRect(
                                      child: Container(
                                        child: Text(
                                          "3/hr",
                                          style: TextStyle(
                                              color: Colors.white
                                          ),
                                        ),
                                        width: 40,
                                        color: Colors.black,
                                      ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                )
                              ],
                            ),
                          );
                        }
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
