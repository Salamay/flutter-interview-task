import 'package:flutter/material.dart';
class RoundButton extends StatelessWidget {
  Function onpressed;
  Color bColor;
  Color tColor;
  String text;
  RoundButton({this.bColor,this.tColor,this.text,this.onpressed});

  @override
  Widget build(BuildContext context) {
    double HEIGTH = MediaQuery.of(context).size.height;
    double WIDTH = MediaQuery.of(context).size.width;
    return GestureDetector(
      child: InkWell(
        splashColor: Colors.grey,
        onTap: onpressed,
        child: Container(
          height: HEIGTH*0.08,
          width: WIDTH,
          margin: EdgeInsets.all(WIDTH*0.1),
          decoration: BoxDecoration(
            color: bColor,
            borderRadius: BorderRadius.all(Radius.circular(20))
          ),
          alignment: Alignment.center,
          child: Text(
            text,
            style: TextStyle(
              color: tColor,
              fontSize: 20,
              fontWeight: FontWeight.bold
            ),
          ),
        ),
      ),
    );
  }
}
