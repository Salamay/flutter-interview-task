import 'package:dog/Model/ListModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const inputdecoration=InputDecoration(
  fillColor: Colors.black12,
  filled: true,

  alignLabelWithHint: true,
  hintStyle: TextStyle(
    color: Colors.grey,
    fontSize: 14
  ),
  labelStyle: TextStyle(
    color: Colors.black,
    fontSize: 18
  ),
  focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(20)),
      borderSide: BorderSide(
      color: Colors.green,
    )
  ),

  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(20)),
    borderSide: BorderSide(
    color: Colors.white,
  )
  )
);

List<ListModel> nearYou=[
  ListModel(image: "assets/Frame33575.png",title: "Mason york",subtitle: "7k from you"),
  ListModel(image: "assets/PHOTO&RAITING.png",title: "Mark grene",subtitle: "10k from you")
];
List<ListModel> Suggested=[
  ListModel(image: "assets/Frame33546.png",title: "Triana",subtitle: "7k from you"),
  ListModel(image: "assets/Frame33553.png",title: "Mark grene",subtitle: "10k from you")
];
