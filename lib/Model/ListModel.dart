import 'dart:core';

class ListModel{
  String image;
  String title;
  String subtitle;
  ListModel({this.image,this.title,this.subtitle});
}