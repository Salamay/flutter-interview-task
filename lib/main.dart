import 'package:dog/Screens/Home.dart';
import 'package:dog/Screens/Onboarding.dart';
import 'package:dog/Screens/SignUp.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        "/onboarding":(context)=>OnBoarding(),
        "/signup":(context)=>SignUp(),
        "/home":(context)=>Home(),
      },
      initialRoute: "/onboarding",
    );
  }
}


